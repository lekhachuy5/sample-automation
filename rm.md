Folder PATH listing for volume Systems
Volume serial number is BC0D-DDF0
C:.
│   .gitignore
│   config.properties
│   pom.xml
│   rm.md
│   
├───.idea
│       .gitignore
│       compiler.xml
│       encodings.xml
│       jarRepositories.xml
│       misc.xml
│       workspace.xml
│       
├───src
│   ├───main
│   │   ├───java
│   │   │   └───org
│   │   │       └───example
│   │   │               Main.java
│   │   │               
│   │   └───resources
│   └───test
│       └───java
└───target
    ├───classes
    │   └───org
    │       └───example
    │               Main.class
    │               
    └───generated-sources
        └───annotations
