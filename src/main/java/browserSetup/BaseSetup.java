package browserSetup;


import org.apache.commons.lang3.BooleanUtils;
import org.openqa.selenium.WebDriver;
import java.io.FileInputStream;
import java.util.Properties;



public class BaseSetup {
    public static WebDriver driver;
    public static Browser browser = new Browser();
    public static Properties properties;

    public static EnvironmentSetup env;

    public BaseSetup() {
    }

    public void setInitialDriver() throws Exception {
        System.out.println("Loading...");
            this.setUpDriver();
            driver.manage().deleteAllCookies();
    }

    private void setUpDriver() throws Exception {
            initialConfigProperties();
            driver = browser.setLocalDriver(env.getBrowserType());
            driver.manage().window().maximize();
            driver.get(env.getWebUrl());
    }

    public void initialConfigProperties() throws Exception{
        if(properties == null) {
            properties = new Properties();
            properties.load( new FileInputStream("src/config.properties"));
        }
        if(env == null) {
            env = new EnvironmentSetup();
            env.setBrowserType(properties.getProperty("type"));
            env.setWebUrl(properties.getProperty("url"));
            env.setHeadless(BooleanUtils.toBoolean(properties.getProperty("headless")));
            env.setJdbcConnection(properties.getProperty("database.jdbcConnectionUrl"));
        }
    }

    public void tearDown() throws Exception {
        driver.quit();
    }
}
