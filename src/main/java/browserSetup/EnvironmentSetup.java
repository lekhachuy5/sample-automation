package browserSetup;

public class  EnvironmentSetup {
    private String webUrl;
    private boolean headless;
    private String browserType;
    private String jdbcConnection;
    public EnvironmentSetup() {
    }
    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    // Getter and Setter for username

    // Getter and Setter for headless
    public boolean isHeadless() {
        return headless;
    }

    public void setHeadless(boolean headless) {
        this.headless = headless;
    }

    // Getter and Setter for browserType
    public String getBrowserType() {
        return browserType;
    }

    public void setBrowserType(String browserType) {
        this.browserType = browserType;
    }
    public void setJdbcConnection(String jdbcConnection) {
        this.jdbcConnection = jdbcConnection;
    }
    public String getJdbcConnection() {
        return jdbcConnection;
    }

}

