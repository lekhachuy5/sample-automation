package browserSetup;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class Browser {
    private static String FIREFOX_BROWSER_NAME = "firefox";
    private static String CHROME_BROWSER_NAME = "chrome";
    private static String EDGE_BROWSER_NAME = "edge";

    public Browser() {
    }

    public WebDriver setLocalDriver(String browserName) throws Exception {
        if (browserName.equalsIgnoreCase(FIREFOX_BROWSER_NAME)) {
            WebDriverManager.firefoxdriver().setup();
            FirefoxBinary firefoxBinary = new FirefoxBinary();
            if (BaseSetup.env.isHeadless()) {
                firefoxBinary.addCommandLineOptions(new String[]{"--headless"});
            }
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setBinary(firefoxBinary);
            if (BaseSetup.env.isHeadless()) {
                WebDriver ff = new FirefoxDriver(firefoxOptions);
                return ff;
            } else {
                return new FirefoxDriver(firefoxOptions);
            }
        } else if (browserName.equalsIgnoreCase(EDGE_BROWSER_NAME)) {
            WebDriverManager.operadriver().setup();
            EdgeOptions edgeOptions = new EdgeOptions();
            if (BaseSetup.env.isHeadless()) {
                edgeOptions.addArguments(new String[]{"headless"});
            }
            if (BaseSetup.env.isHeadless()) {
                WebDriver ed = new EdgeDriver(edgeOptions);
                return ed;
            } else {
                return new EdgeDriver(edgeOptions);
            }
        } else {
            if (System.getProperty("webdriver.chrome.driver") == null) {
                WebDriverManager.chromedriver().setup();
            }
            WebDriverManager.chromedriver().setup();
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments(new String[]{"--disable-site-isolation-trials"});
            if (BaseSetup.env.isHeadless()) {
                chromeOptions.addArguments(new String[]{"--headless"});
            }
            if (BaseSetup.env.isHeadless()) {
                WebDriver ch = new ChromeDriver(chromeOptions);
                return ch;
            } else {
                return new ChromeDriver(chromeOptions);
            }
        }
    }
}
