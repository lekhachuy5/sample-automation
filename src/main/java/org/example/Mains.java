package org.example;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import browserSetup.BaseSetup;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.CommonUtils;
import utils.DatabaseUtils;

public class Mains {
    public File excel = new File("src/main/java/org/example/excel_test.xlsx");
    public FileInputStream fis;
    public XSSFWorkbook workbook;
    public WebDriver driver;
    public XSSFSheet sheet;

    public Mains() throws IOException {
        fis = new FileInputStream(excel);
        workbook = new XSSFWorkbook(fis);
        sheet = (XSSFSheet) workbook.getSheet("Sheet1");
        driver = BaseSetup.driver;
    }

    public Sheet sheet1;

    @Before
    public void setUp() throws Exception {
        BaseSetup bs = new BaseSetup();
        bs.setInitialDriver();
        driver = BaseSetup.driver;
    }

    @Test
    public void readExcel() throws Exception {

        XSSFCell cell1 = sheet.getRow(0).getCell(0);
        XSSFCell cell2 = sheet.getRow(1).getCell(1);
        System.out.println("Cell 1: " + cell1);
        System.out.println("Cell 2: " + cell2);
        workbook.close();
        fis.close();
        WebElement user = driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        user.sendKeys(cell2.toString());
        System.out.println(Integer.parseInt(getData("testRow", "testCol")) + 2);

    }

    @Test
    public void databaseCall() throws Exception {
//        File sqlFile = new File("src/main/java/org/example/simpleSQL.sql");
//        String sqlString = IOUtils.toString(new FileReader(sqlFile));
//        System.out.println( DatabaseUtils.executeQuery(sqlString));
        Map<String,String> query = CommonUtils.getSQLStatement("src/main/java/org/example/sql_test.sql");
        System.out.println("\n Result:: "+ DatabaseUtils.executeQuery(query.get("--beforeTest")));
//        System.out.println("\n Result:: "+ DatabaseUtils.executeQuery("Select 'this is test query' as huy"));
    }

    public String getData(String rowName, String colName) throws IOException {
        int columnIdx = -1;
        int rowIdx = -1;
        // Tìm vị trí của column header
        XSSFRow headerRow = sheet.getRow(0);
        for (int i = 0; i <= headerRow.getLastCellNum(); i++) {
            XSSFCell cell = headerRow.getCell(i);
            if (cell != null && cell.getStringCellValue().equals(colName)) {
                columnIdx = cell.getColumnIndex();
                break;
            }
        }
        // Tìm vị trí của row header
        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            XSSFRow row = sheet.getRow(i);
            if (row != null) {
                XSSFCell cell = row.getCell(0);
                if (cell != null && cell.getStringCellValue().equals(rowName)) {
                    rowIdx = row.getRowNum();
                    break;
                }
            }
        }
        // Lấy giá trị của ô dựa trên vị trí columnIdx và rowIdx
        if (columnIdx != -1 && rowIdx != -1) {
            XSSFRow row = sheet.getRow(rowIdx);
            XSSFCell cell = row.getCell(columnIdx);
            String cellValue = null;
            try {
                cell.setCellType(CellType.STRING);
                cellValue = cell.getStringCellValue();
            } catch (Exception e) {
                System.out.println("Invalid cell type");
            }
            return cellValue;
        } else {
            return null;
        }
    }

    @After
    public void tearDown() throws Exception {
        driver.close();
    }

}