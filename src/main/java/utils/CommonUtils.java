package utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CommonUtils {

    public CommonUtils(){}

    public static Map<String,String> getSQLStatement(String filePath){
        HashMap sqlStatements = new HashMap();
        String key = null;
        try {
            File sqlFile = new File(filePath);
            String sqlString = IOUtils.toString(new FileReader(sqlFile));
            String[] currSqlArr = sqlString.split("--");
            for (int i = 0; i < sqlString.split("--").length; i++) {
                String statement = currSqlArr[i].toString();
                if (statement.length() > 0) {
                    key = statement.substring(0, statement.indexOf('\n')).trim();
                    sqlStatements.put(key, statement.substring(statement.indexOf('\n') + 1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sqlStatements.keySet());
        return sqlStatements;
    }
    public static String replaceTimestamp(String text) throws Exception {
        if (text.matches("(?s).*\\{TIMESTAMP_.*\\}.*")) {
            int startIndex = text.indexOf("{TIMESTAMP_") + "{TIMESTAMP_".length();
            int endIndex = text.indexOf("}", startIndex);
            String format = text.substring(startIndex, endIndex);
            return text.replaceAll("\\{TIMESTAMP_" + format + "\\}", getCurrentDate(format));
        } else if (StringUtils.contains(text, "{TIMESTAMP}")) {
            return StringUtils.replace(text, "{TIMESTAMP}", (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()));
        } else {
            return StringUtils.contains(text, "${timestamp}") ? StringUtils.replace(text, "${timestamp}", (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime())) : text;
        }
    }

    public static String replaceRandomNumber(String text) throws Exception {
        if (text.matches("(?s).*\\{NUMBER_.*\\}.*")) {
            int startIndex = text.indexOf("{NUMBER_") + "{NUMBER_".length();
            int endIndex = text.indexOf("}", startIndex);
            String length = text.substring(startIndex, endIndex);
            return text.replaceAll("\\{NUMBER_" + length + "\\}", generateRandomNumber((long)Integer.parseInt(length)));
        } else {
            return text;
        }
    }

    public static String replaceRandomText(String text) throws Exception {
        if (text.matches("(?s).*\\{TEXT_.*\\}.*")) {
            int startIndex = text.indexOf("{TEXT_") + "{TEXT_".length();
            int endIndex = text.indexOf("}", startIndex);
            String length = text.substring(startIndex, endIndex);
            return text.replaceAll("\\{TEXT_" + length + "\\}", generateRandomString(Integer.parseInt(length)));
        } else {
            return text;
        }
    }

    public static String replaceDynamicData(String text) throws Exception {
        text = replaceTimestamp(text);
        text = replaceRandomNumber(text);
        text = replaceRandomText(text);
        return text;
    }

    public static final String generateRandomString(int len) {
        String RandomCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder sb = new StringBuilder();
        Random rnd = new Random();

        while(sb.length() < len) {
            int index = (int)(rnd.nextFloat() * (float)RandomCHARS.length());
            sb.append(RandomCHARS.charAt(index));
        }

        return sb.toString();
    }
    public static final String generateRandomNumber(long len) {
        if (len > 18L) {
            throw new IllegalStateException("To many digits");
        } else {
            long tLen = (long)Math.pow(10.0D, (double)(len - 1L)) * 9L;
            long number = (long)(Math.random() * (double)tLen) + (long)Math.pow(10.0D, (double)(len - 1L)) * 1L;
            String tVal = number + "";
            if ((long)tVal.length() != len) {
                throw new IllegalStateException("The random number '" + tVal + "' is not '" + len + "' digits");
            } else {
                return tVal;
            }
        }
    }
    public static String getCurrentDate(String format) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(cal.getTime());
    }
}
