package utils;

import browserSetup.BaseSetup;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.plexus.util.IOUtil;

public class DatabaseUtils {
    private static String MSSQL_DB_CONN = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static Connection connection = null;
    private static Statement statement = null;
    private static ResultSet result = null;
    private static int resultRowCount = 0;
    private static List<String> outputs = new ArrayList();

    public final static Logger log = (Logger) LogManager.getLogger();

    public DatabaseUtils() {
    }

    public static void initConnection() throws Exception {
        Class.forName(MSSQL_DB_CONN);
        connection = DriverManager.getConnection(BaseSetup.env.getJdbcConnection());
        statement = connection.createStatement();
    }

    public static Object executeQuery(String query) throws Exception {
        initConnection();
        System.out.println("Query string: "+ query);
        List<List<Map<String, String>>> resultSetsData = new ArrayList(); // Khai báo biến resultSetsData để lưu trữ dữ liệu từ ResultSet
        PreparedStatement statementLocal = null; // Khai báo biến statementLocal để thực hiện truy vấn
        try {
            ResultSet rs = null; // Khai báo biến rs để lưu trữ kết quả của truy vấn
            statementLocal = connection.prepareStatement(query, 1004, 1007); // Chuẩn bị câu truy vấn
            boolean results = statementLocal.execute(); // Thực thi câu truy vấn và lấy kết quả

            while (true) {
                ArrayList records; // Biến records dùng để lưu trữ dữ liệu từ ResultSet
                if (!results) {
                    if (statementLocal.getUpdateCount() == -1) { // Kiểm tra nếu không có dữ liệu trả về từ câu truy vấn INSERT, UPDATE hoặc DELETE
                        records = (ArrayList) resultSetsData; // Gán dữ liệu từ resultSetsData cho records
                        return records; // Trả về dữ liệu
                    }
                    results = statementLocal.getMoreResults(); // Di chuyển đến kết quả truy vấn tiếp theo
                } else {
                    rs = statementLocal.getResultSet(); // Lấy ResultSet từ kết quả truy vấn
                    if (!rs.next()) {
                        rs.close();
                        results = statementLocal.getMoreResults(); // Di chuyển đến kết quả truy vấn tiếp theo
                    } else {
                        records = new ArrayList();
                        ResultSetMetaData rsmd = rs.getMetaData(); // Lấy thông tin metadata của ResultSet
                        do {
                            Map<String, String> data = new HashMap();

                            for (int j = 1; j <= rsmd.getColumnCount(); ++j) {
                                data.put(rsmd.getColumnName(j), rs.getString(rsmd.getColumnName(j)) != null ? rs.getString(rsmd.getColumnName(j)).trim() : rs.getString(rsmd.getColumnName(j)));
                                // Lưu trữ giá trị từng cột vào Map, với key là tên cột và value là giá trị tương ứng
                            }

                            records.add(data);
                        } while (rs.next()); // Di chuyển đến hàng dữ liệu tiếp theo trong ResultSet

                        rs.close();
                        results = statementLocal.getMoreResults();
                        if (!results && resultSetsData.size() == 0) {
                            statementLocal.close();
                            ArrayList noRecords = records;
                            return noRecords;
                        }

                        resultSetsData.add(records);
                    }
                }
            }
        } finally {
            if (statementLocal != null) {
                statementLocal.close();
            }
            closeConnection();
        }
    }



    public static void closeConnection() throws Exception {
        try {
            if (result != null) {
                result.close();
                log.info("Oracle driver connection's resultset closed");
            }

            if (statement != null) {
                statement.close();
                log.info("Oracle driver connection's statement closed");
            }

            if (connection != null) {
                connection.close();
                log.info("Oracle driver connection is closed.");
            }

        } catch (SQLException sqlExceptionError) {
            System.out.println("SQL connection error: " + sqlExceptionError.getMessage());
            throw sqlExceptionError;
        }
    }
}
